package main

import (
	"bitbucket.org/bobrofon/tpr_solver/criterions"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
)

// ReadTask decodes json file specified by name src into dst.
// "-" file name describes stdio.
func ReadTask(src string, dst interface{}) {
	var in *os.File
	if src == "-" {
		in = os.Stdin
	} else {
		file, err := os.Open(src)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()
		in = file
	}

	dec := json.NewDecoder(in)
	if err := dec.Decode(dst); err != nil {
		log.Fatal(err)
	}
}

func main() {
	inputFilePtr := flag.String("i", "-", "input file")
	criterionPtr := flag.String("c", "", "decision criteria")
	printCriterionListPtr := flag.Bool("l", false, "Print a list of decision criterions and exit.")

	flag.Parse()

	if *printCriterionListPtr {
		for _, element := range criterions.List() {
			fmt.Println(element)
		}
		return
	}

	solver, err := criterions.New(*criterionPtr)
	if err != nil {
		log.Fatal(err)
	}

	ReadTask(*inputFilePtr, solver.Task())
	solver.Solve()

	fmt.Printf("Solution: pure strategies with numbers %v. Record: %v\n", solver.Solutions(), solver.Record())
}
