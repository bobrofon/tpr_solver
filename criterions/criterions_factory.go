package criterions

import (
	"errors"
)

type SolverConstructor func() Solver

var (
	solvers = make(map[string]SolverConstructor)
)

type Solver interface {
	// Task returns pointer to task object inside the Solver.
	Task() interface{}
	// Solve generates the solution and the record.
	// Task should be specified.
	Solve()
	Record() float64
	Solutions() []int
}

func register(solverName string, constructor SolverConstructor) {
	solvers[solverName] = constructor
}

// New returns solver registered with name solverName.
func New(solverName string) (Solver, error) {
	if constructor, ok := solvers[solverName]; ok {
		return constructor(), nil
	} else {
		return nil, errors.New("Unknown criterion: " + solverName)
	}
}

// List returns list of registered solvers.
func List() []string {
	keys := []string{}
	for k := range solvers {
		keys = append(keys, k)
	}
	return keys
}
