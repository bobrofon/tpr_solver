package criterions

import (
	"bitbucket.org/bobrofon/tpr_solver/utils"
)

type MultiplicationTask struct {
	CostMatrix [][]float64
}

type Multiplication struct {
	BaseSolver
	task MultiplicationTask
}

func (self *Multiplication) Task() interface{} {
	return &self.task
}

func (self *Multiplication) Solve() {
	mul := make([]float64, len(self.task.CostMatrix))
	for i, row := range self.task.CostMatrix {
		mul[i] = 1
		for _, v := range row {
			mul[i] *= v
		}
	}

	self.record = utils.MaxInArray(mul)
	self.solution = utils.EqualId(mul, self.record)
}

func init() {
	register("multiplication", func() Solver {
		return new(Multiplication)
	})
}
