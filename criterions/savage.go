package criterions

import (
	"bitbucket.org/bobrofon/tpr_solver/utils"
)

type SavageTask struct {
	CostMatrix [][]float64
}

type Savage struct {
	BaseSolver
	task SavageTask
}

func (self *Savage) Task() interface{} {
	return &self.task
}

func (self *Savage) Solve() {
	maxInColumn := utils.MaxInColumn(self.task.CostMatrix)
	for _, row := range self.task.CostMatrix {
		for i := range row {
			row[i] = maxInColumn[i] - row[i]
		}
	}

	maxInRow := utils.MaxInRow(self.task.CostMatrix)
	self.record = utils.MinInArray(maxInRow)
	self.solution = utils.EqualId(maxInRow, self.record)
}

func init() {
	register("savage", func() Solver {
		return new(Savage)
	})
}
