package criterions

import (
	"bitbucket.org/bobrofon/tpr_solver/utils"
)

type GermeierTask struct {
	CostMatrix  [][]float64
	Probability []float64
}

type Germeier struct {
	BaseSolver
	task GermeierTask
}

func (self *Germeier) Task() interface{} {
	return &self.task
}

func (self *Germeier) validate() {
	if len(self.task.Probability) != len(self.task.CostMatrix[0]) {
		self.task.Probability = make([]float64, len(self.task.CostMatrix[0]))
		p := 1 / float64(len(self.task.Probability))
		for i := range self.task.Probability {
			self.task.Probability[i] = p
		}
	}
}

func (self *Germeier) Solve() {
	self.validate()

	for _, row := range self.task.CostMatrix {
		for i := range row {
			row[i] *= self.task.Probability[i]
		}
	}

	min := utils.MinInRow(self.task.CostMatrix)
	self.record = utils.MaxInArray(min)
	self.solution = utils.EqualId(min, self.record)
}

func init() {
	register("germeier", func() Solver {
		return new(Germeier)
	})
}
