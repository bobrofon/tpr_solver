package criterions

import (
	"bitbucket.org/bobrofon/tpr_solver/utils"
)

type HurwitzTask struct {
	CostMatrix [][]float64
	Pessimism  float64
}

type Hurwitz struct {
	BaseSolver
	task HurwitzTask
}

func (self *Hurwitz) Task() interface{} {
	return &self.task
}

func (self *Hurwitz) Solve() {
	min := utils.MinInRow(self.task.CostMatrix)
	max := utils.MaxInRow(self.task.CostMatrix)

	c := make([]float64, len(min))
	for i := range c {
		c[i] = self.task.Pessimism*min[i] + (1-self.task.Pessimism)*max[i]
	}

	self.record = utils.MaxInArray(c)
	self.solution = utils.EqualId(c, self.record)
}

func init() {
	register("hurwitz", func() Solver {
		return new(Hurwitz)
	})
}
