package criterions

import (
	"bitbucket.org/bobrofon/tpr_solver/utils"
)

type WaldTask struct {
	CostMatrix [][]float64
}

type Wald struct {
	BaseSolver
	task WaldTask
}

func (self *Wald) Task() interface{} {
	return &self.task
}

func (self *Wald) Solve() {
	minArray := utils.MinInRow(self.task.CostMatrix)
	self.record = utils.MaxInArray(minArray)

	self.solution = utils.EqualId(minArray, self.record)
}

func init() {
	register("wald", func() Solver {
		return new(Wald)
	})
}
