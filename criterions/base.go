package criterions

type BaseSolver struct {
	record   float64
	solution []int
}

func (self *BaseSolver) Record() float64 {
	return self.record
}

func (self *BaseSolver) Solutions() []int {
	return self.solution
}
