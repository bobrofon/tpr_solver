package criterions

import (
	"bitbucket.org/bobrofon/tpr_solver/utils"
)

type BayesTask struct {
	CostMatrix  [][]float64
	Probability []float64
}

type Bayes struct {
	BaseSolver
	task BayesTask
}

func (self *Bayes) Task() interface{} {
	return &self.task
}

func (self *Bayes) validate() {
	if len(self.task.Probability) != len(self.task.CostMatrix[0]) {
		self.task.Probability = make([]float64, len(self.task.CostMatrix[0]))
		p := 1 / float64(len(self.task.Probability))
		for i := range self.task.Probability {
			self.task.Probability[i] = p
		}
	}
}

func (self *Bayes) Solve() {
	self.validate()

	aq := make([]float64, len(self.task.CostMatrix))
	for i, row := range self.task.CostMatrix {
		aq[i] = 0
		for j, v := range row {
			aq[i] += self.task.Probability[j] * v
		}
	}

	self.record = utils.MaxInArray(aq)
	self.solution = utils.EqualId(aq, self.record)
}

func init() {
	register("bayes", func() Solver {
		return new(Bayes)
	})
}
