package utils

import (
	"math"
)

type MinMaxFunc func(float64, float64) float64

func InArray(a []float64, f MinMaxFunc) float64 {
	res := a[0]
	for _, v := range a {
		res = f(res, v)
	}
	return res
}

func MaxInArray(a []float64) float64 {
	return InArray(a, math.Max)
}

func MinInArray(a []float64) float64 {
	return InArray(a, math.Min)
}

func InRow(a [][]float64, f MinMaxFunc) []float64 {
	res := make([]float64, len(a))
	for i, row := range a {
		res[i] = row[0]
		for _, v := range row {
			res[i] = f(res[i], v)
		}
	}
	return res
}

func MaxInRow(a [][]float64) []float64 {
	return InRow(a, math.Max)
}

func MinInRow(a [][]float64) []float64 {
	return InRow(a, math.Min)
}

func InColumn(a [][]float64, f MinMaxFunc) []float64 {
	res := make([]float64, len(a[0]))
	copy(res, a[0])
	for _, row := range a {
		for i, v := range row {
			res[i] = f(res[i], v)
		}
	}
	return res
}

func MaxInColumn(a [][]float64) []float64 {
	return InColumn(a, math.Max)
}

func MinInColumn(a [][]float64) []float64 {
	return InColumn(a, math.Min)
}

// EqualId returns array of positions in a[] where value of a[] equals x.
func EqualId(a []float64, x float64) []int {
	res := make([]int, 0, len(a))
	for i, v := range a {
		if v == x {
			res = append(res, i+1)
		}
	}
	return res
}
